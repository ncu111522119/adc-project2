from module.sqlORM.sqlClass import FileList
from module.sqlORM.MyCRUD import  SearchFileSrc
from module.saveFile.saveToSameFloder import Set_FileList_withSameFloder
from module.dataHidingSteganography.Embedding import Embedding
from module.dataHidingSteganography.Extraction import Extraction
from flask import Flask, send_from_directory, request, abort, send_file
from flask_cors import CORS
from dotenv import load_dotenv
import uuid
from markupsafe import escape
import os
import time
import io
# === initial variable ====
load_dotenv()  # 載入 .env


# ==== inital Setting =====
app = Flask(__name__, static_folder='react_app/build')  # create the app
CORS(app, resources={r"/*": {"origins": "*", "allow_headers": "*", "expose_headers": "*"}})
# Serve React App
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def serve(path):
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')


# ========= Function =============

# ========= API Route ============
# @app.route("/")
# def hello_world():
#     return "<p>Hello, World!</p>"

## 嵌入檔案上傳
## => Input : <FormData: FileType> Image、Txt
## => Output: FileID
@app.route('/Embedding/upload', methods=['GET', 'POST'])
def Embedding_upload_file():
    if request.method == 'POST':
        print(request.files)
        req_file_image = request.files['imageFile']
        req_file_txt = request.files['txtFile']
        if req_file_image.filename != "" and req_file_txt.filename != "":
            ThisfileID = str(uuid.uuid1())
            ## 圖片存檔
            FileSaveStauts = Set_FileList_withSameFloder("Upload/Embedding", ThisfileID, {"Image":req_file_image,"Secret":req_file_txt})
            
            if FileSaveStauts["Status"] == "Success":
                ## 檔案嵌入於圖片之中
                EmbeddingStatus = Embedding(FileSaveStauts["FilePath"]["Secret"], FileSaveStauts["FilePath"]["Image"], "/Embedding")
            
            ## 判斷回傳前端狀態
            if FileSaveStauts["Status"] == "Success" and EmbeddingStatus["Status"] == "Success":
                # time.sleep(5)
                return {"Status": "Success", "EmbeddingImageID": EmbeddingStatus["FileID"], "EmbedingURL": "/getFile/Image/"+EmbeddingStatus["FileID"]}
            elif FileSaveStauts["Status"] == "Failed":
                abort(500, FileSaveStauts["Msg"])
                # return {"Status": "Failed", "Msg": FileSaveStauts["Msg"]}                
            elif EmbeddingStatus["Status"] == "Failed":
                abort(500, EmbeddingStatus["Msg"])
                # return {"Status": "Failed", "Msg": EmbeddingStatus["Msg"]}
        else:
            abort(500, "無檔案上傳，請確認檔案狀態")






## 待萃取圖片檔案上傳
## => Input : <FormData: FileType> Image、Txt
## => Output: FileID
@app.route('/Extraction/upload', methods=['GET', 'POST'])
def Extraction_upload_file():
    if request.method == 'POST':
        req_file = request.files['imageFile']
        if req_file.filename != "":
            ThisfileID = str(uuid.uuid1())
            FileSaveStauts = Set_FileList_withSameFloder("Upload/Extraction", ThisfileID, {"Embedding":req_file})
            
            if FileSaveStauts["Status"] == "Success":
                ## 檔案嵌入於圖片之中
                ExtractionStatus = Extraction( FileSaveStauts["FilePath"]["Embedding"], "/Extraction/")

            if FileSaveStauts["Status"] == "Success" and ExtractionStatus["Status"] == "Success":
                return {"Status": "Success", "Secret":  ExtractionStatus["Secret"]}
                # return {"Status": "Success", "FileID": ExtractionStatus["FileID"], "ExtractionURL": "/getFile/Image/"+FileSaveStauts["FileID"]}
            elif ExtractionStatus["Status"] == "Failed" :
                return {"Status": "Failed", "Msg":  ExtractionStatus["Msg"]}
            else:
                abort(500, "檔案上傳失敗，資料庫新增異常")
        else:
            abort(500, "無檔案上傳，請確認檔案狀態")

## 圖片檔案萃取出文字
## => Input: FileID
## => Output: Txt File

## 根據FileID 取得 資料表中對應的 檔案路徑
@app.route('/getFile/Image/<FileID>', methods=['GET', 'POST'])
def getImage(FileID):
    if request.method == 'GET':
        FileSrc = SearchFileSrc(FileID, FileList)
        if FileSrc["Status"] == "Success":
            try:
                with open(FileSrc["value"], 'rb') as bites:
                    return send_file(
                        io.BytesIO(bites.read()),
                        mimetype='image/jpg'
                    )
            except Exception as err:
                abort(500, "出了一點問題 > <'")
                # return {"Status": "Failed", "Msg": err}
        else:
            abort(404, "找不到此圖片 > <")


@app.route('/getFile/DownloadImage/<FileID>', methods=['GET', 'POST'])
def getImage_Download(FileID):
    if request.method == 'GET':
        FileSrc = SearchFileSrc(FileID, FileList)
        if FileSrc["Status"] == "Success":
            try:
                print(FileSrc["value"])
                return send_file(FileSrc["value"], as_attachment=True)
            except Exception as err:
                abort(500, "出了一點問題 > <'")
                # return {"Status": "Failed", "Msg": err}
        else:
            abort(404, "找不到此圖片 > <")
