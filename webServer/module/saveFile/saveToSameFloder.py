from module.sqlORM.sqlClass import FileList
from module.sqlORM.MyCRUD import Insert
from dotenv import load_dotenv
import os

load_dotenv()
# 將多個檔案以ID 方式儲存於 相同ID的資料夾之中
'''
- File Path <Floder>
-- 00842d50-73de-11ed-9029-d45d64413e7e
--- 00842d50-73de-11ed-9029-d45d64413e7e.png
--- 00842d50-73de-11ed-9029-d45d64413e7e.txt
==========================================
Input:
--
1.Floder <Str> = FilePath
2.FileID <Str> = UUID
3.req_file_array = <dictionary or JSON> ， HTTP Request Form File
'''


def Set_FileList_withSameFloder(Floder, FileID, req_file_array):
    # 用於 FileList 類別資料
    FileArray = []
    thisFilePath = {}
    for req_file_key in req_file_array:
        thisFileID = FileID + "_"+req_file_key
        # 取得及設定相應變數
        ThisFileType = req_file_array[req_file_key].filename.split(".")[-1]
        ThisfileName = f'{thisFileID}.{ThisFileType}'
        ThisSrc = os.getenv("FilePath")+f'/{Floder}/{ThisfileName}'
        # 設定 資料列的型別
        FileDataRow = FileList(
            id=thisFileID,
            fileName=ThisfileName,
            src=ThisSrc
        )
        # 將設定好的資料存入陣列之中
        FileArray.append(FileDataRow)
        print("ThisfileName: ", ThisfileName)
        # 存檔
        req_file_array[req_file_key].save(ThisSrc)

        thisFilePath[req_file_key] = ThisSrc

    # 更新 至資料表之中
    Status = Insert(FileArray)["Status"]

    # 判斷是否 新增成功
    if Status == "Success":
        return {"Status": "Success", "FileID": FileID, "FilePath": thisFilePath}
    else:
        Msg = "------ Func:Set_FileList_withSameFloder ------"+"\n"
        Msg += "檔案上傳失敗，資料庫新增異常"+"\n"
        Msg += "===== Func:Set_FileList_withSameFloder End ========"
        print(Msg)
        return {"Status": "Failed", "Msg": Msg}


def Set_FileList_withSameFloder_withPython(Floder, FileID, ThisFileType, req_file_array):
    print(FileID)
    # 用於 FileList 類別資料
    FileArray = []
    thisFilePath = {}
    for req_file_key in req_file_array:
        thisFileID = FileID + "_"+req_file_key
        # 取得及設定相應變數
        ThisfileName = f'{thisFileID}.{ThisFileType}'
        ThisSrc = os.getenv("FilePath")+f'/{Floder}/{ThisfileName}'
        # 設定 資料列的型別
        FileDataRow = FileList(
            id=thisFileID,
            fileName=ThisfileName,
            src=ThisSrc
        )
        # 將設定好的資料存入陣列之中
        FileArray.append(FileDataRow)
        print("ThisfileName: ", ThisfileName)
        # 存檔
        req_file_array[req_file_key].save(ThisSrc)

        thisFilePath[req_file_key] = ThisSrc

    # 更新 至資料表之中
    Status = Insert(FileArray)["Status"]

    # 判斷是否 新增成功
    if Status == "Success":
        return {"Status": "Success", "ImageID": thisFileID, "FilePath": thisFilePath}
    else:
        Msg = "------ Func:Set_FileList_withSameFloder ------"+"\n"
        Msg += "檔案上傳失敗，資料庫新增異常"+"\n"
        Msg += "===== Func:Set_FileList_withSameFloder End ========"
        print(Msg)
        return {"Status": "Failed", "Msg": Msg}


def Extraction_SecretTxt_withPython(Floder, FileID, ThisFileType, extracted_plaintext):
    thisFileID = FileID+'_extracted'
    ThisfileName = thisFileID+ThisFileType
    thisFilePath = os.getenv("FilePath")+Floder+ ThisfileName 
    # 設定 資料列的型別
    FileDataRow = FileList(
        id=thisFileID,
        fileName=ThisfileName,
        src=thisFilePath
    )
    # 更新 至資料表之中
    Status = Insert([FileDataRow])["Status"]

    # 判斷是否 新增成功
    if Status == "Success":
        try:
            with open(thisFilePath, 'w', encoding="utf-8") as f:
                f.write(extracted_plaintext)
            f.close()
        except Exception as err:
            print("err", err)
            return {"Status": "Failed", "Msg": "資料庫新增失敗"}
        return {"Status": "Success", "FileID": thisFileID, "FilePath": thisFilePath}
    else:
        Msg = "------ Func:Set_FileList_withSameFloder ------"+"\n"
        Msg += "檔案上傳失敗，資料庫新增異常"+"\n"
        Msg += "===== Func:Set_FileList_withSameFloder End ========"
        print(Msg)
        return {"Status": "Failed", "Msg": Msg}
