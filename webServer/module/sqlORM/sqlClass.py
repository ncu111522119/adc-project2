import sqlalchemy as db
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy_utils import database_exists, create_database
import os
from dotenv import load_dotenv

# === initial variable ====

Base = declarative_base()

         

## FileList 表的格式
class FileList(Base):
    __tablename__ = "FileList"
    id = db.Column(db.String(50), primary_key=True)
    fileName = db.Column(db.String(55), nullable=False)
    src = db.Column(db.String(256), nullable=False)
    
        

if __name__ == '__main__':
    ## 載入 .env
    load_dotenv() 
    ## 連接資料庫
    engine = create_engine(f'mysql+pymysql://{os.getenv("hostname")}:{os.getenv("passwd")}@127.0.0.1:3306/{os.getenv("db_name")}', echo=True, future=True)
    ## 建立資料庫
    if not database_exists(engine.url):
        create_database(engine.url)
    ## 創建資料表
    Base.metadata.create_all(engine)

