import sqlalchemy as db
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
import os
from dotenv import load_dotenv

# === initial variable ====
## 載入 .env
load_dotenv() 
## 連接資料庫
engine = create_engine(f'mysql+pymysql://{os.getenv("hostname")}:{os.getenv("passwd")}@127.0.0.1:3306/{os.getenv("db_name")}', echo=True, future=True)

## 新增
def Insert(DataRow):
    with Session(engine) as session:
        try:
            session.add_all(DataRow)
            session.commit()
        except Exception as err:
            print("Error:\n")
            print(err)
            return {"Status": "Failed", "Msg": err}
    return {"Status": "Success", "Msg": "新增成功" }

## 查詢
def SearchFileSrc(fileID, FileList):
        session = Session(engine)
        try:
            stmt = db.select(FileList.src).where(FileList.id == fileID)
            stmt = session.scalars(stmt).one()
            return {"Status": "Success", "value": stmt}
        except Exception as err:
            return {"Status": "Failed", "Msg": err}