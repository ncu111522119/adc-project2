# adc-project2/API_Server

## Getting started

### Original

> $ python -m flask --app main run --host=0.0.0.0 -p 8080

![Debug Off](./Image/runStart_DebugOff.png)

#### Parameter

* -m flask: 來執行
* --app: 執行檔案
* --host: 需使用 = 來設定 host IP
* -p : 設定 Port 號， --port

### Debug Mode is Open

> $ python -m flask --app main --debug run --host=0.0.0.0 -p 8080

![Debug On](./Image/runStart_DebugOff.png)

#### Parameter
* --debug: 必須在 run 前面 !

## .env: 環境配置檔
step1: 新建.env，請於 webServer 底下新建  .env

step2: .env 中新增: 
```
 host = '127.0.0.1'
 hostname = 【資料庫帳號】
 passwd = 【資料庫密碼】
 db_name = 'ADL_Project2' 
 FilePath = 【放置專案的路徑】 + "/adc-project2/ webServer/File"
 Upload = 【放置專案的路徑】 +  "/adc-project2/ webServer/File/Upload/"
```
## Installation
### 創建 Mysql 資料表
```
$ python ./module/sqlORM/sqlClass.py
```

## Description

## Package

* [Flask](https://flask.palletsprojects.com/en/1.1.x/quickstart/)
* [flask-sqlalchemy](https://docs.sqlalchemy.org/en/14/orm/quickstart.html)
* sqlalchemy-utils
* dotenv

## Project Arcitecture

* `main.py` Server 進入點，主要執行的Python 檔案
* `Z_ReadMe` 存放 Readme 與 相關圖片
* `.env` 儲存隱私資料，相關常用的環境變數
* `module` main.py會使用到的func
  * `dataHidingSteganography` 圖片密文隱藏，圖片萃取密文 func
  * `saveFile` 儲存檔案的 func ，主要處理 Http OR Python 圖片處理 與 獲取 檔案ID並記錄至DB Table 之中
  * `sqlORM` SQL ORM 相關 func，主要內容為 FileList Table，檔案ID 對應 Path
* `File` 存放圖片或文字檔的Floder
  * `Embedding` 以嵌入完成的圖片
  * `Extraction` 解析出來的Secret，.txt檔案
  * `upload` 上傳的圖片與文字存放區
    * `Embedding` 嵌入時所需的檔案，主要內容為: 圖片、文字檔
    * `Extraction` 萃取密文時所需檔案，主要內容: 含有密文的圖像
